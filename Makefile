%.stl: %.scad
	openscad $< -o $@

DocuCam-Clip-%.scad: DocuCam-%.scad
	sed "s/^set.*/back();/" $< > $@

DocuCam-Mirror.scad: DocuCam.scad
	sed "s/^set.*/front();/" $< > $@

DocuCam-%.scad: DocuCam.scad
	sed "s/monitor_thickness = [0-9]*;/monitor_thickness = $$(echo $* | sed 's/.*-//');/" $< > $@

all: \
	DocuCam-3-5.stl   DocuCam-Clip-3-5.stl \
	DocuCam-4-6.stl   DocuCam-Clip-4-6.stl \
	DocuCam-5-7.stl   DocuCam-Clip-5-7.stl \
	DocuCam-6-8.stl   DocuCam-Clip-6-8.stl \
	DocuCam-7-9.stl   DocuCam-Clip-7-9.stl \
	DocuCam-8-10.stl  DocuCam-Clip-8-10.stl \
	DocuCam-9-11.stl  DocuCam-Clip-9-11.stl \
	DocuCam-10-12.stl DocuCam-Clip-10-12.stl \
	DocuCam-11-13.stl DocuCam-Clip-11-13.stl \
	DocuCam-12-14.stl DocuCam-Clip-12-14.stl \
	DocuCam-13-15.stl DocuCam-Clip-13-15.stl \
	DocuCam-14-16.stl DocuCam-Clip-14-16.stl \
	DocuCam-Mirror.stl

