# DocuCam Laptop Document Camera

This design was inspired by [John
Umekubo](https://twitter.com/jumekubo/status/1301969450564898816)'s
pocket document camera. His was designed in Tinkercad and sets the
mirror at a fixed angle. This design allows you to adjust the camera
angle, including flipping the mirror out of the way but leaving
it installed. It can be customized for your particular laptop
[in Thingiverse](https://www.thingiverse.com/thing:4590755) or in
[OpenSCAD](https://www.openscad.org/), and the model is meant to
be reasonably easy to read and modify by other OpenSCAD users.

The default 2" mirror size fits [a set of 2" craft mirrors sold in
sets of 60 at Amazon](https://www.amazon.com/gp/product/B07T8YJQZS).
Other sizes exist and this design is intended to adjust reasonably
to other mirror sizes within reason. Thick glass mirrors, however,
will probably result in "ghosting" on the resulting image as there
will be reflections off both the front surface and the rear mirror
surface, as well as a green tint that will be more pronounced the
thicker the glass.

## Instructions

You can either use the `DocuCam-#-#.stl` files that have already
been built, or you can generate your own STL files from the
source, which ever you prefer.

### Starting from STL

Measure the thickness of the laptop lid near the camera, either
(carefully) with calipers, or by printing out `MonitorLidGauge.stl`
and noting the number next to the smallest gap that fits.

Choose the `DocuCam-#-#.stl` file that has your monitor thickness
close to the middle. For example, if you monitor measures approximately
6mm with the calipers, or fits to the "6" slot on the gauge, print
the `DocuCam-5-7.stl` file.

### Rebuilding in OpenSCAD

Measure the thickness of the monitor around the camera.  If you
already have calipers, that's a good choice. Be careful not to
scratch the monitor surface.  This may be an appropriate use for
cheap plastic calipers.

Measure the mirror. "Height" and "Width" are of course interchangeable
depending on your preference and on optical limitations. Thickness
may be approximate, must be thick enough to have a "registration"
box into which the mirror is set, and should not be a lot thicker
than the mirror. The 1mm default is appropriate for most mirrors
that are appropriate for this setup.

A wide variation in mirror sizes has not been tested.  If you change
the mirror sizes substantially from the nominal 50mm / 2" square
mirrors, you should check to make sure that the design still works.

## Printing

I print in PETG with 20% infill and .3mm layers. The layers are
actually used as a ratcheting retention device, so do not smooth
the layers out around the axle.  PLA+/PLAPro is likely to work,
as is ABS. Normal PLA may be too brittle.

Snap the two halves together, oriented as printed and moved toward
each other. The parts should splay apart a bit; this is by design
to hold the angle you set.

Use double-sized tape, expoxy, or super glue to attach the mirror
only to the center of the space. Don't fill the whole space with
glue.  The mirror should fit inside the registration box.

Gently slip the back piece onto the top of the monitor, centered
on the calendar, making sure not to obscure or damage the camera.

Then, since you purchased a 60-pack of mirrors, make 59 more of
these for your friends, family, and all their teachers.  Just get
measurements for their laptops first!  The gauges cost mere pennies
to print.

## Troubleshooting

If rotation is too loose or too tight, alter the `interference`
parameter.

If the mirror doesn't fit, the plastic may have shrunk. Measure the
difference and add it to the size, or just scale the object in the
slicer.

# Contributing

Please feel free to file issues and propose Merge Requests with
improvements at [GitLab](https://gitlab.com/mcdanlj/docucam)

Please feel to fork, remix, or be inspired by this work.

Copyright Michael K Johnson
https://creativecommons.org/licenses/by/4.0/ CC BY 4.0

