// Adjustable mirror for laptop camera as document camera
// Copyright Michael K Johnson
// https://creativecommons.org/licenses/by/4.0/ CC BY 4.0

/* [Mirror Measurement] */

// Measure your mirror in one dimension, add about 1mm for clearance
mirror_width = 51.8;
// Measure your mirror in the other dimension, add about 1mm for clearance
mirror_height = 51.8;
// Measure actual mirror thickness, but at least 0.3 for common slicer configuration
mirror_thickness = 1; // [0.3:5]

/* [Computer measurement] */
// Maximum supported monitor thickness; intended to work with up to 2mm thinner monitor lids
monitor_thickness = 7;

/* [Adjustments] */

// If the prints are too hard to snap together at all, or if adjustment is too stiff, increase this; if the adjustment is too loose to hold, decrease it
interference = .15; // [0:0.5]

// If the mirror is too far down, move it up by adding to this offset in mm; if it is too far up, this can be a negative number
monitor_bezel_offset = 0;

/* [Hidden] */
// If you need to adjust these parameters, you probably need to be in OpenSCAD not customizer, and be reading the code

monitor_variance = 2; // monitor_thickness - monitor_variance is minimum supported thickness
monitor_back_height = 30;
shell = 2;
mirror_bezel = 5;
axle_radius = 10;
fillet_r = 1;

nub_d = 10;
nub_scale = 0.75;

total_y = mirror_width + 2*mirror_bezel;
camera_width = mirror_width - 2* mirror_bezel; // cutout for camera to see through
back_x = monitor_thickness + shell + monitor_variance + axle_radius;
total_front_thickness = shell + mirror_thickness;
inside_mirror_offset = sqrt( 2*axle_radius*(total_front_thickness)
                             - pow(total_front_thickness, 2)) + shell;
front_x = mirror_height + mirror_bezel;

pivot_h = mirror_bezel;
pivot_offset = camera_width/2 + pivot_h/2;
tang_w = 3 * pivot_h;
tang_thickness = 3;
tang_pressure = 1; // extra distance beyond monitor_variance thinnest supported bezel for positive pressure
tang_foot = 4;

$fn=60;

module nubs(extra=0) {
    // Snap to hold the rotating parts concentric
    for (i=[-1, 1]) {
        translate([0, i*mirror_width/2, axle_radius-shell/2])
            scale([1, nub_scale, 1]) sphere(d=nub_d+extra);
    }
}

module lid_proxy(increment) {
    translate([-(axle_radius+monitor_thickness), -150, shell/2+increment])
        cube([monitor_thickness, 300, 200]);
}

module back() {
    bezel_z = axle_radius+monitor_bezel_offset;
    //%lid_proxy(bezel_z);
    difference() {
        union() {
            // bottom plate
            translate([-back_x/2, 0, 0])
                cube([back_x, total_y, shell], center=true);
            // monitor back flange
            translate([shell/2-back_x, 0, shell])
                cube([shell, total_y, shell*2], center=true);
            for (i=[-1, 1]) {
                // monitor offset
                translate([-axle_radius+shell, i*(pivot_offset+pivot_h/2), shell/2+bezel_z/2])
                    cube([axle_radius, pivot_h*2, bezel_z], center=true);
                y = i*pivot_offset;
                hull() {
                    // axle body
                    translate([0, y, axle_radius-shell/2]) rotate([90, 0, 0])
                        cylinder(r=axle_radius, h=pivot_h, center=true);
                    translate([-(axle_radius-fillet_r), y, axle_radius*2.25-2*fillet_r]) rotate([90, 0, 0])
                        cylinder(r=fillet_r, h=pivot_h, $fn=15, center=true);
                }
                // screen holder bezel tangs
                t = tang_thickness;
                x = monitor_variance+tang_pressure-(axle_radius+monitor_thickness+t/2);
                hull() {
                    // top of bezel tang foot
                    translate([x, y, bezel_z+axle_radius+tang_foot]) rotate([90, 0, 0])
                        cylinder(d=t, h=tang_w, $fn=15, center=true);
                    // middle of bezel tang
                    translate([x, y, bezel_z+axle_radius-tang_foot]) rotate([90, 0, 0])
                        cylinder(d=t, h=tang_w, $fn=15, center=true);
                }
                hull() {
                    // middle of bezel tang
                    translate([x, y, bezel_z+axle_radius-tang_foot]) rotate([90, 0, 0])
                        cylinder(d=t, h=tang_w, $fn=15, center=true);
                    // bottom of bezel tang
                    translate([-back_x, y, t/2-shell/2]) rotate([90, 0, 0])
                        cylinder(d=t, h=tang_w, $fn=15, center=true);
                }
            }
        }
        // remove space for camera from axle
        translate([0, 0, axle_radius-shell/2]) rotate([90, 0, 0])
            cylinder(r=axle_radius+interference, h=camera_width, center=true);
        // remove front axle interference fillet
        for (i=[-1, 1]) {
            translate([0, i*(total_y/2 - mirror_bezel/2 + .1), axle_radius-shell/2]) rotate([90, 0, 0])
                cylinder(r=axle_radius+interference, h=mirror_bezel+.2, center=true);
            // remove useless fillet
            translate([0, i*(total_y/2), 0])
                cube([axle_radius* .7, mirror_bezel*2, shell*2], center=true);

        }
        // remove useless fillet
        cube([inside_mirror_offset*2, camera_width, shell*2], center=true);
        // label the intended thickness
        translate([-(axle_radius+1), 0, shell/2-0.6]) rotate([0, 0, 90])
            linear_extrude(1)
            text(str(monitor_thickness-monitor_variance, " - ", monitor_thickness), halign="center", size=6);
    }
    nubs();
}
    
module front() {
    difference() {
        union() {
            // base
            translate([front_x/2 + inside_mirror_offset, 0, mirror_thickness/2])
                cube([front_x, total_y, shell+mirror_thickness], center=true);
            // axles
            for (i=[-1, 1]) {
                y = i*(total_y/2 - mirror_bezel/2);
                // axle
                translate([0, y, axle_radius-shell/2]) rotate([90, 0, 0])
                    cylinder(r=axle_radius, h=mirror_bezel, center=true);
                hull() {
                    // attach
                    translate([axle_radius*2, y, 0]) rotate([90, 0, 0])
                        cylinder(d=shell, h=pivot_h, center=true, $fn=12);
                    translate([axle_radius-shell/2, y, axle_radius-shell/2]) rotate([90, 0, 0])
                        cylinder(d=shell, h=pivot_h, center=true, $fn=12);
                    translate([shell, y, axle_radius/2]) rotate([90, 0, 0])
                        cylinder(d=shell, h=pivot_h, center=true, $fn=12);
                    translate([shell, y, axle_radius/2]) rotate([90, 0, 0])
                        cylinder(d=shell, h=pivot_h, center=true, $fn=12);
                    translate([mirror_bezel + shell*2, y, 0]) rotate([90, 0, 0])
                        cylinder(d=shell, h=pivot_h, center=true, $fn=12);
                }
            }
        }
        // remove space for nubs
        nubs(interference*2);
        // remove space for mirror
        translate([inside_mirror_offset, -mirror_width/2, shell/2])
           cube([mirror_height, mirror_width, mirror_thickness*2]);
    }
}

module set(offset=0) {
    translate([-offset, 0, 0]) back();
    translate([offset, 0, 0]) front();
}

translate([0, 0, shell/2])
set(axle_radius+2);
