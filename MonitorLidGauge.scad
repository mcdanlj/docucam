// Monitor lid gauge
// Copyright Michael K Johnson
// https://creativecommons.org/licenses/by/4.0/ CC BY 4.0

difference() {
    step=6;
    start=2;
    end=8;
    cube([end+step*1.6, (2+end-start)*step, 1.5]);
    for(x=[start:end]) {
        n=1+x-start;
        translate([step, step*n, -.1]) cube([x, step, 2]);
        translate([step*0.8, step*n, 0.9]) linear_extrude(1) text(str(x), size=step-1, halign="right");
    }
}